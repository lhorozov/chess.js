const WHITE_TILE_COLOR = '#F0D9B5';
const BLACK_TILE_COLOR = '#B58863';
const letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
const numbers = [1, 2, 3, 4, 5, 6, 7, 8];
const FIGURES = {
    bPawn: '&#9823;',
    bRook: '&#9820;',
    bKnight: '&#9822;',
    bBishop: '&#9821;',
    bQueen: '&#9819;',
    bKing: '&#9818;',

    wPawn: '&#9817;',
    wRook: '&#9814;',
    wKnight: '&#9816;',
    wBishop: '&#9815;',
    wQueen: '&#9813;',
    wKing: '&#9812;'
};

const tileSize = 70;

let clickedPiece = '';

const body = document.getElementsByTagName('body')[0];
const board = document.createElement('div');
board.style.width = tileSize * 8 + 'px';
board.className = 'board';
body.appendChild(board);
board.addEventListener('click', (event) => {
    const tiles = document.querySelectorAll('.tile');
    [].forEach.call(tiles, function (tile) {
        tile.classList.remove('clicked');
    });

    if (!clickedPiece && event.target.parentNode.classList.contains('tile')) {
        const clickedTile = event.target.parentNode;
        clickedTile.classList.add('clicked');
        clickedPiece = clickedTile;
    } else {
        event.target.className = clickedPiece.childNodes[1].className;
        event.target.innerHTML = clickedPiece.childNodes[1].innerHTML;
        clickedPiece.childNodes[1].className = '';
        clickedPiece.childNodes[1].innerHTML = '';
        clickedPiece = '';
    }
});

renderBoard();
addPieces();

function renderBoard() {
    for (let n = numbers.length - 1; n >= 0; n--) {
        for (let l = 0; l < letters.length; l++) {
            if (l % 2 === 0) {
                if (n % 2 === 0) {
                    addTile(board, BLACK_TILE_COLOR, letters[l] + numbers[n]);
                } else {
                    addTile(board, WHITE_TILE_COLOR, letters[l] + numbers[n]);
                }
            } else {
                if (n % 2 === 0) {
                    addTile(board, WHITE_TILE_COLOR, letters[l] + numbers[n]);
                } else {
                    addTile(board, BLACK_TILE_COLOR, letters[l] + numbers[n]);
                }
            }
        }
    }
    return board;
}

function addPieces() {
    [].forEach.call(board.childNodes, function (tile) {
        const piece = document.createElement('div');
        piece.style.width = tileSize + 'px';
        piece.style.height = tileSize + 'px';
        
        const [letter, number] = tile.textContent.split('');
        if (number === '2') {
            piece.innerHTML = FIGURES.wPawn;
            piece.className = 'piece wPawn';
        }
        else if (number === '7') {
            piece.innerHTML = FIGURES.bPawn;
            piece.className = 'piece bPawn';
        }
        else if (letter + number === 'A1' || letter + number === 'H1' ) {
            piece.innerHTML = FIGURES.wRook;
            piece.className = 'piece wRook';
        }
        else if (letter + number === 'B1' || letter + number === 'G1' ) {
            piece.innerHTML = FIGURES.wKnight;
            piece.className = 'piece wKnight';
        }
        else if (letter + number === 'C1' || letter + number === 'F1' ) {
            piece.innerHTML = FIGURES.wBishop;
            piece.className = 'piece wBishop';
        }  
        else if (letter + number === 'D1') {
            piece.innerHTML = FIGURES.wQueen;
            piece.className = 'piece wQueen';
        } 
        else if (letter + number === 'E1') {
            piece.innerHTML = FIGURES.wKing;
            piece.className = 'piece wKing';
        }
        else if (letter + number === 'A8' || letter + number === 'H8' ) {
            piece.innerHTML = FIGURES.bRook;
            piece.className = 'piece bRook';
        } 
        else if (letter + number === 'B8' || letter + number === 'G8' ) {
            piece.innerHTML = FIGURES.bKnight;
            piece.className = 'piece bKnight';
        }
        else if (letter + number === 'C8' || letter + number === 'F8' ) {
            piece.innerHTML = FIGURES.bBishop;
            piece.className = 'piece bBishop';
        } 
        else if (letter + number === 'D8') {
            piece.innerHTML = FIGURES.bQueen;
            piece.className = 'piece bQueen';
        } 
        else if (letter + number === 'E8') {
            piece.innerHTML = FIGURES.bKing;
            piece.className = 'piece bKing';
        } 
        tile.appendChild(piece);
    });
}

function addTile(appendTo, color, position) {
    const tile = document.createElement('div');
    color === BLACK_TILE_COLOR
        ? (tile.className = `tile black ${position}`)
        : (tile.className = `tile white ${position}`);
    const p = document.createElement('p');
    p.textContent = position;
    tile.appendChild(p);
    tile.style.width = tileSize + 'px';
    tile.style.height = tileSize + 'px';
    tile.style.background = color;
    appendTo.appendChild(tile);
}
